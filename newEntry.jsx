import React from 'react';
import superagent from "superagent";
import csv from 'csvtojson';
class NewEntry extends React.Component {
    constructor(props) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
    }
    handleChange(evt) {
      var files = evt.target.files;
      var filename = evt.target.files[0].name;
      for (var i = 0, f; f = files[i]; i++) {
        var reader = new FileReader();
        let callback = this.props.callback;
        reader.onload = (function(theFile) {
          return function(e) {
            var jsonobj;
            superagent.post('/api/csv_validate')
            .send({
              fileName:filename,
              data:e.target.result
            })
            .end(function(err, res) {
              if (err) {
                console.log("error", err);
                callback(err+"#"+e.target.result);
              } else{
                console.log(res);
                callback(res.text+"#"+e.target.result);
              }
            })
          };
        })(f);
        reader.readAsText(f);
      }
    }
    render() {
      return (
        <div class="card">
            <h2>New Entry<hr/></h2>
            <h4>Upload The CSV File<br/><h5 style={{opacity:0.5}}>Please upload the goodwill entry data file</h5></h4>
            <input class="choosefile" type="file" id="files" accept=".csv" name="files[]" multiple onChange={ (e) => this.handleChange(e) }/>
        </div>
      );
   }
}
export default NewEntry;