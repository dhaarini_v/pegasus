var fs = require('fs');
var mongo = require('mongodb').MongoClient;
var Binary = require('mongodb').Binary;
import dateFormat from 'dateformat';
exports.insertLog=function(fileName,userdetails,purchaseStatus,data,callback2){
    var filePath="./uploads/"+fileName;
    var csvdump = fs.readFileSync(filePath);
    var info=data.split(',');
    var no_of_records=info[0];
    var amountCredited=info[1];
    var document = {};
    var fileDump = Binary(csvdump);
    var fileSize=fileDump.length;
    var now=new Date();
    var date=now.getTime();
    document={
        uploadedby:userdetails.email,
        filepath:fileName,
        file:csvdump,
        filesize:csvdump.length+"bytes",
        noOfRecords:no_of_records,
        creditAmount:amountCredited,
        time:date,
        status:purchaseStatus
    }
    mongo.connect('mongodb://localhost:27017/',function(err,db) {
        var response;
        if(err){
            response={
                message:'Database connection error'
            };
            return callback2(response,"error");
        }
        var dbobj=db.db('logon');
        dbobj.collection('logonCl').insert(document, function(err, doc){
            if(err){
                response={
                    message:'Record insertion error'
                };
                return callback2(response,"error");
            }
            else{
                response={
                    message:'Information logged on successfully'
                }
                return callback2(null,response);
            }
        });
    });
}