import React from 'react';
import superagent from 'superagent';
var username;
class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state={username:"signin"};
    }
    componentDidMount(){
        superagent.get("/api/username")
                  .end((err, res) => {
                      username=res.body.name;
                    //   console.log("username:"+username);
                      this.setState({userName:username});
                    });
    }
    render() {
        // console.log("header loading"+this.state.userName);
        return (
           <div class="header2">
           <div class='logowrap'>
           <img class="logo" src="https://logos-download.com/wp-content/uploads/2016/09/Myntra_logo.png"/>
           </div>
           &nbsp;&nbsp;&nbsp;&#9776;&nbsp;&nbsp;Goodwill Refunds
           {/* <span class='modulename'>Goodwill Refunds</span> */}
           <div class="userwrap">
               <button class="userbutton"> {username} <i class="fa fa-caret-down"></i></button>
               <div class="dropdown-content">
                    <a href="/logout">Logout</a>
                </div>
           </div>
           </div>
       );
    }
}
export default Header; 