import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import NewEntry from './newEntry.jsx';
import Preview from './preview.jsx';
import {withRouter} from 'react-router-dom';
import superagent from "superagent";
var refundsData=[];
class AllRefund extends React.Component {
    constructor(props) {
        super(props);
        this.state = {btnClicked : false,shared_var:'Please upload the file to see the preview'};
        this.handleClick = this.handleClick.bind(this);
    }
    updateShared(value){
        this.setState({shared_var: value});
        
    }
    handleClick() {
        this.setState({btnClicked:false});
    }
    componentDidMount(){
        superagent.get("/api/data")
                  .end((err, res) => {
                      this.setState({result: res.body})
                    });
    }
    render() {
        const options = {
            page: 1, 
            sizePerPageList: [ {
              text: '5', value: 5
            }, {
              text: '10', value: 10
            },{
                text: '20', value: 20
            },{
                text: '50', value: 50
            }], 
            sizePerPage: 10,  
            pageStartIndex: 1, 
            paginationSize: 6,  
            prePage: '<', 
            nextPage: '>', 
            firstPage: '<<', 
            lastPage: '>>',
            withFirstAndLast: true
          };
        if(this.state.btnClicked === false){
            return (
                <div>
                    <div class="comp2">
                            <h2 class="allrefunds">All Refunds 
                            <button   class="addnew" onClick={() => {this.setState({btnClicked:true});}}>ADD NEW</button>
                            </h2>
                    </div>
                    <div class="bcard">
                    <div class="btable">
                    <BootstrapTable data={ this.state.result} options={ options } bordered={ false } pagination>
                        <TableHeaderColumn dataField='filepath' isKey={ true } width='200' tdStyle={ { whiteSpace: 'normal' } }>Name of the File</TableHeaderColumn>
                        <TableHeaderColumn dataField='time' width='200' tdStyle={ { whiteSpace: 'normal' } }>Uploaded on</TableHeaderColumn>
                        <TableHeaderColumn dataField='uploadedby' width='300' tdStyle={ { whiteSpace: 'normal' } }>Uploaded by</TableHeaderColumn>
                        <TableHeaderColumn dataField='noOfRecords' width='160' tdStyle={ { whiteSpace: 'normal' } }>Records Uploaded</TableHeaderColumn>
                        <TableHeaderColumn dataField='creditAmount' width='120' tdStyle={ { whiteSpace: 'normal' } }>Total Amount</TableHeaderColumn>
                        <TableHeaderColumn dataField='status' width='120' tdStyle={ { whiteSpace: 'normal' } }>Credit Status</TableHeaderColumn>
                    </BootstrapTable>
                    </div>
                    </div>
                </div>
            );
        }else{
            return (
        
                <div>
                    <div class="comp2">
                        <h5 class="link"><a href='' class="backto" onClick={this.handleClick}>All Refunds</a> >> New Entry</h5>
                        <h2>New Goodwill Entry</h2>
                    </div>
                    < NewEntry callback={this.updateShared.bind(this)}/>
                    < Preview shared_var={this.state.shared_var}/>
                </div>
            );
        }
      
   }
}
export default withRouter(AllRefund);