var fs = require("fs");
var async = require('async')
var csv = require('csv-validator');
exports.validateCsv = function(filePath,data,callback1){
    var response;
    var writestream = fs.createWriteStream(filePath);
    writestream.write(data);
    writestream.end();
    const headers = {
        ppsId: /^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/,
        uidxId: /^[0-9a-f]{8}\.[0-9a-f]{4}\.[0-9a-f]{4}\.[0-9a-f]{4}\.[0-9a-zA-Z]{22}$/, 
        refundReason: /^[a-zA-Z ]+$/,
        amount: /^\d+$/,
        comment:/^[a-zA-Z ]+$/
    };
    var err;
    csv(filePath, headers)
    .then(function(done){
        response={
            message:'File validated'
        };
        return callback1(null,response);       
    })
    .catch(function(error){
        response={
            message:error
        };
        return callback1(response,"error");
    });
}