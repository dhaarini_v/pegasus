var Request = require("request");
exports.callApi=function(ppsId,uidxId,reason,amt,comment,callback1){
  Request.post({
    "headers": { "Cache-Control": "no-cache",
      "content-type": "application/json",
      "Postman-Token":"6977cfdc-63f9-2e46-5a40-0b47e496ae74"},
    "url": "http://giftcard.stage.myntra.com/giftcard-service/v2/giftcardpurchase/purchase",
    "body": JSON.stringify({
        "clientId": ppsId,
        "login":uidxId,
        "cardProgramName":reason,
        "amount":amt,
        "additionalParams": {
            "ppsId": null,
            "orderId": null,
            "returnId": null,
            "comments":comment,
            "ppsTransactionId": ppsId
        }
    })
  },function(err,res){
    var response;
      if(err){
        console.log(err)
        response={
          message:"Error in giftcard purchase\n"+err
        };
        return callback1(response,"error");
      }else{
        response={
          message:'Giftcard purchase successful'
        };
        return callback1(null,response);
      }
  });
}