module.exports = {
    services: {
        sso: {
            root: 'http://unitysso.stage.myntra.com'
        },
        security: {
            root: 'http://security.stage.myntra.com'
        }
    }
}