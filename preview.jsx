import React from 'react';
import superagent from 'superagent';
import { ToastContainer, toast } from 'react-toastify';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Papa from 'papaparse';
var jsonobj,noOfErrors,noOfRecords;



class Preview extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.timeout = this.timeout.bind(this);
        this.csvJSON=this.csvJSON.bind(this);
        this.csverror=this.csverror.bind(this);
        // this.state={noOfErr:0};
    }
    timeout(){
        window.location.reload();
    }
    handleClick(){
        var resultdata=this.props.shared_var.split(/\n|#/);
        console.log("redultdata",resultdata);
        var filename=resultdata[1];

        superagent.post('/api/csv_upload')
            .send({
              fileName:filename,
            })
            .end(function(err, res) {
              if (err) {
                // console.log("error", JSON.stringify(err));
                toast("Giftcards purchased failed");
              } else{
                // console.log("response",JSON.stringify(res));
                toast("Giftcards purchased successfully");
              }
            })
            
            setTimeout(this.timeout,5000);
            
    }
    csvJSON() {
        var data=this.props.shared_var;
        data=data.split('#');
        var csv=data[1];
        var json=Papa.parse(csv,{header:true});
        var jsonObj=json.data;
        noOfRecords=jsonObj.length;
        // console.log("nor"+jsonObj.length);
        return JSON.stringify(jsonObj); //JSON
    }
    csverror(){
        var data=this.props.shared_var;
        data=data.split('#');
        var error=data[1];
        var rows=error.split(/,|:/);
        noOfErrors=rows.length/2;
        var cnt;
        var csv=data[2];
        var json=Papa.parse(csv,{header:true});
        var jsonObj=json.data;
        noOfRecords=jsonObj.length;
        var rowno,fieldname;
        var resultObj=[];
        for(cnt=0;cnt<rows.length;cnt++){
            rowno=parseInt(rows[cnt],10)-1;
            // console.log(JSON.stringify(jsonObj[rowno]));
            cnt=cnt+1;
            fieldname=rows[cnt];
            jsonObj[rowno][fieldname]=jsonObj[rowno][fieldname]+'~*';
            jsonObj[rowno]["rowno"]=rowno+1;
            resultObj.push(jsonObj[rowno]);
        }
        // console.log(JSON.stringify(resultObj))
        return resultObj;
    }
    errorComponent(cell, row){
        if(cell.includes("~*")){
            var content=cell.split('~');
            return <span>{content[0]} <i class="fa fa-warning" style={{color:"red"}}/></span>
        }else{
            return <span>{cell}</span>
        }
    }
    render() {
        var response=this.props.shared_var;
        var resultdata=this.props.shared_var;
        resultdata=resultdata.split('\n');
        if(resultdata[0]=="Success"){
            var json=this.csvJSON();
            jsonobj=JSON.parse(json);
            jsonobj=jsonobj.slice(0,10);
            return (
                <div>
                    <div class="card">
                        <h2>Preview<button class="success" disabled>{noOfRecords} / {noOfRecords}</button></h2><hr/>
                        <BootstrapTable data={jsonobj} striped headerStyle={ { background: '#cccccc' } } bordered={ false }>
                        <TableHeaderColumn dataField='ppsId' isKey={ true } width='250' tdStyle={ { whiteSpace: 'normal' } }>PPSID</TableHeaderColumn>
                            <TableHeaderColumn dataField='uidxId' width='420' tdStyle={ { whiteSpace: 'normal' } }>UIDXID</TableHeaderColumn>
                            <TableHeaderColumn dataField='refundReason' width='200' tdStyle={ { whiteSpace: 'normal' } }>REFUND REASON</TableHeaderColumn>
                            <TableHeaderColumn dataField='amount' width='120' tdStyle={ { whiteSpace: 'normal' } }>AMOUNT</TableHeaderColumn>
                            <TableHeaderColumn dataField='comment'width='200' tdStyle={ { whiteSpace: 'normal' } }>COMMENT</TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                    <div class="uploadcsv">
                    <button type="submit" class="publish" onClick={this.handleClick}>Publish</button>
                    <ToastContainer />
                    </div>
                </div>
            );
        }else if(resultdata[0]=="Error"){
            console.log(JSON.stringify(this.props.shared_var));
            var json=this.csverror();
            //jsonobj=JSON.parse(json);
            jsonobj=json.slice(0,10);
            return (
                <div>
                    <div class="card">
                    <h2>Preview<button class="error" disabled> {noOfErrors} / {noOfRecords}</button></h2><hr/>
                        <BootstrapTable data={jsonobj} striped headerStyle={ { background: '#cccccc' } } bordered={ false }>
                            <TableHeaderColumn dataField='rowno' isKey={ true } width='90' tdStyle={ { whiteSpace: 'normal' } }>ROWNO</TableHeaderColumn>
                            <TableHeaderColumn dataField='ppsId' width='200' tdStyle={ { whiteSpace: 'normal' } } dataFormat={this.errorComponent}>PPSID</TableHeaderColumn>
                            <TableHeaderColumn dataField='uidxId' width='420' tdStyle={ { whiteSpace: 'normal' } } dataFormat={this.errorComponent}>UIDXID</TableHeaderColumn>
                            <TableHeaderColumn dataField='refundReason' width='200' tdStyle={ { whiteSpace: 'normal' } } dataFormat={this.errorComponent}>REFUND REASON</TableHeaderColumn>
                            <TableHeaderColumn dataField='amount' width='100' tdStyle={ { whiteSpace: 'normal' } } dataFormat={this.errorComponent}>AMOUNT</TableHeaderColumn>
                            <TableHeaderColumn dataField='comment'width='200' tdStyle={ { whiteSpace: 'normal' } } dataFormat={this.errorComponent}>COMMENT</TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                    <div class="uploadcsv">
                    <button type="submit" class="publish" onClick={this.handleClick} disabled>Publish</button>
                    </div>
                </div>
            );
        }else{
            return (
                <div>
                    <div class="card">
                        <h2>Preview</h2><hr/>
                            <h5 style={{opacity:0.5}}>{this.props.shared_var}</h5>
                    </div>
                    <div class="uploadcsv">
                        <button type="submit" class="publish" disabled>Publish</button>
                    </div>
                </div>
            );
        }
   }
}
export default Preview;