import App from './app.jsx';
import index from './index.js';
import React from 'react';
import wares from 'unity-middlewares';
import ucontext from 'unity-context';
import { renderToString } from 'react-dom/server';
import _ from 'underscore';
var express = require('express');
var fs=require('fs');
var app = express();
var bodyParser = require('body-parser');
var multer  = require('multer');
app.use(express.static('static'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb',extended: true}));
var upload = multer({});
import getRefunds from './services/getRefunds';
import async from 'async';
import validate from './services/validateCsv';
var dumpfile = require('./services/filedump');
var giftcardApi = require("./services/giftcardApi")
var fcsv=require("fast-csv");
import csv from 'csvtojson';
var filePath="";
var fileName="";
var userdetails;
var configData = require("unity-config").init({
    dir: __dirname + "/config",
    name: 'config'
})

wares.configure(app, {
    rootDir: __dirname,
    config: configData,
    sanitize: { rootPath: "/app/proxy", pick: ['services'] }
})

function sanitize(config, options) {
    //get the config and make all service root url relative to /app/proxy on the browser side
    let pick = options.pick || ['browser', 'services', 'hostname'];
    let browserConfig = JSON.parse(JSON.stringify(_.pick(config, pick)));
    if (browserConfig.services) {
        Object.keys(browserConfig.services).forEach(function(item) {
            if (browserConfig.services[item].root)
                browserConfig.services[item].root = options.rootPath || '/app/proxy';
            if (browserConfig.services[item].auth)
                browserConfig.services[item].auth = '';
        });
    }
    return browserConfig;
}


app.get("/api/data", function(req, res){
    getRefunds.getAllRefunds(function(err,result){
        if(err){
            console.log("error");
            res.send(err);
        }else{
            res.send(result);
        }
    })
})
app.get("/api/username", function(req, res){

    res.send(userdetails);
})
app.post("/api/csv_validate", function(req, res){
    fileName=req.body.fileName;
    filePath = "./uploads/"+fileName;
    async.waterfall(
        [
            function(callback) {
                validate.validateCsv(filePath,req.body.data,function(err,result){
                    if(err){
                        console.log("\nAfter csv validation:\n"+err.message+"\n");
                        callback(err.message,"Error");
                    }else{
                        console.log("\nAfter csv validation:\n"+result.message+"\n");
                        callback(null,result.message);
                    } 
                });
            }
        ],
        function (err, caption) {
            if(err) {
                res.setHeader('Content-Type', 'text/html');
                res.write('Error\n');
                res.write('#'+err);
                res.end();
            }else{
                console.log(caption);
                res.setHeader('Content-Type', 'text/html');
                res.write('Success\n' + fileName);
                res.end();
            }
        }
    );
})
app.post('/api/csv_upload', function (req, res) {
    fileName=req.body.fileName;
    filePath = "./uploads/"+fileName;
    var noOfrecords,amount,data,status,purchase;
    var dumpStatus;
    async.waterfall(
        [
            function(callback){
                var records=-1;
                var creditamt=0;
                let csvstream = fcsv.fromPath(filePath)
                    .on('data', function (row) {
                        csvstream.pause();
                        if(records!==-1){
                            creditamt=creditamt+parseInt(row[3]);
                        }
                        records = records + 1;
                        csvstream.resume();
                    })
                    .on('end',function(){
                        callback(null,records+","+creditamt);
                    })
                    .on('error', function (error) {
                        callback("error","error");
                    });
            },
            function(info,callback) {
                data=info.split(',');
                noOfrecords=data[0];
                amount=data[1];
                var flag;
                console.log("Calling giftcard api:\n");
                let csvstream = fcsv.fromPath(filePath)
                    .on('data', function (row) {
                        csvstream.pause();
                        giftcardApi.callApi(row[0],row[1],row[2],row[3],row[4],function(err,result){
                            if(err){
                                flag=true;
                            }
                        });
                        csvstream.resume();
                    })
                    .on('error', function (error) {
                        console.log(error)
                    });
                if(flag!==true){
                    status="Success";
                    console.log(status);
                }else{
                    status="Error";
                    console.log(status);
                }
                var caption =status;
                callback(null, caption,noOfrecords+','+amount);
            },
            function(purchaseStatus,info,callback) {
                purchase=purchaseStatus;
                dumpfile.insertLog(fileName,userdetails,status,info,function(err,result){
                    if(err){
                        dumpStatus=err.message;
                        console.log("After db insertion:\n"+dumpStatus+"\n");
                        if(purchase==="Error"){
                            callback(purchaseStatus+". "+dumpStatus,"purchaseError");
                        }else{
                            callback(dumpStatus,"dbError");
                        }
                    }else{
                        console.log("After db insertion:\n"+result.message+"\n");
                        dumpStatus=result.message;
                        if(purchase==="Error"){
                            callback(purchaseStatus,"purchaseError");
                        }else{
                            callback(null,dumpStatus);
                        }
                    }
                });
               
            }
        ],
        function (err, caption) {
            if(err) {
                res.setHeader('Content-Type', 'text/html');
                res.write('<br>FileName: ' + fileName + '\n<br>');
                // res.write('File Size:'+ fileSize +' bytes\n<br>');
                if(caption==="dbError"){
                    res.write('Giftcard purchase successful for '+ noOfrecords+' records<br>')
                    res.write(err);
                }else{
                    res.write(err);
                }
                res.end();
            }else{
                    res.setHeader('Content-Type', 'text/html');
                    res.write('<br>FileName: ' + fileName + '\n<br>');
                    // res.write('File Size:'+ fileSize +' bytes\n<br>');
                    res.write('Giftcard purchase successful for '+ noOfrecords+' records');
                    res.end();
            }
        }
    );
});
app.use(function(req, res, next) {
    let xcontext = ucontext(req);
    xcontext.init({
        rootDir: __dirname,
        assetsManifest: 'dist/manifest.json'
    });
    xcontext.setConfig(configData, sanitize(configData, {}));
    console.log('Environment is ', process.env.NODE_ENV);
    xcontext.set('env', process.env.NODE_ENV);
    next();
});

app.use(wares.chain({ skipAccessCheck: true }));

    app.use('/logout', [
        wares.chain({ logout: true }),
        function(req, res, next) {
            res.cookie('campaInfo', '', { maxAge: -10000, httpOnly: false });
            res.redirect('/');
        }
    ]);

app.use(function(req, res, next){
    req.user = ucontext(req).get("user");
    // console.log("user Details", req.user);
    userdetails=req.user;
    next()
});

app.get('/', function (req, res) {
    res.send(index({
        body: renderToString(<App/>)
    }));
});
app.get('/paymentOffers', function (req, res) {
    res.send(index({
        body: renderToString(<App/>)
    }));
});

var server = app.listen(9001, function () {
    var host = server.address().address
    var port = server.address().port
    
    console.log("App listening at http://%s:%s", host, port)
 });