import React from 'react';
import { Link, Route, Switch,BrowserRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import { StaticRouter } from 'react-router';
import AllRefund from './allRefunds.jsx';
import paymentOffers from './paymentOffers.jsx';
import Header from './header.jsx';
class App extends React.Component {
   render() {
    // console.log("----------------------++++++++++++++"+JSON.stringify(data));
      return (
        <div>
            <Header/>
        <StaticRouter>
            <div class="row2">
                <div class="leftcolumn2">
                    <ul class="nav2">
                        
                        <li class="navitem2"><Link class="active" to={'/'} >Goodwill Refunds</Link></li>
                        {/* <li class='navitem2'><Link to={'/paymentOffers'}>Payment Offers</Link></li> */}
                    </ul>
                </div>
                <div class="rightcolumn2">
                <Switch>
                  <Route exact path='/' component={AllRefund} />
                  {/* <Route path='/paymentOffers' component={paymentOffers} /> */}
               </Switch>
                </div>
            </div>
        </StaticRouter>
        </div>
      );
   }
}
if (typeof window !== 'undefined') {
    ReactDOM.render(
      <BrowserRouter><App /></BrowserRouter>,
        document.getElementById('root')
      );   
}
export default App;