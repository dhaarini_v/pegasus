var MongoClient = require('mongodb').MongoClient;
import dateFormat from 'dateformat';
var url = "mongodb://localhost:27017/";
exports.getAllRefunds=function(callbck){
    MongoClient.connect(url, function(err, db) {
        var response;
        if(err){
            response={
                message:'Database connection error'
            };
            return callbck(response,"error");
        }
        var dbo = db.db("logon");
        dbo.collection("logonCl").find({}).project({_id:0,"uploadedby":1,"filepath":1,"noOfRecords":1,"creditAmount":1,"time":1,"status":1}).sort({time:-1}).toArray(function(err, result) {
            if(err){
                response={
                    message:'Data fetch error'
                };
                return callbck(response,"error");
            }
            else{
                for(var i in result){
                    var now=new Date(result[i].time);
                    var date=dateFormat(now, "hh:MM:sstt | d mmm 'yy ");
                    result[i].time=date;
                }
                return callbck(null,result);
            }
        });
    });
}