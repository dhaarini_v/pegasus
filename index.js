export default ({ body }) => {
    // console.log('the body is ',body);
    return  `
        <!DOCTYPE html />
        <html>
        <head>
            <meta charset = "UTF-8"/>
            <link rel="shortcut icon" href="favicon.ico" />
            <link rel="stylesheet" href="./bootstrap.min.css"/>
            <link rel="stylesheet" href="./react-bootstrap-table.min.css"/>
            <link rel="stylesheet" href="./component.css" />
            <link rel="stylesheet" href="./ReactToastify.css" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <title>React App</title>
        </head>
        <body>
            <hr style="margin:0px;"/>
            <div id="root">${body}</div>
            <script src="./main.js"></script>
        </body>
        </html>
    `;
};
