const path = require('path');

var config = {
   entry: './main.js',
   output: {
        path: path.resolve(__dirname, 'static'),
        publicPath: '/static',
        filename: '[name].js',
   },
   devServer: {
      inline: true,
      port: 8081
   },
   module: {
      rules: [
         {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
               presets: ['es2015', 'react']
            }
         },
         {
            test: /\.(gif|png|jpe?g|svg)$/i,
            use: [
                'file-loader',
                {
                loader: 'image-webpack-loader',
                options: {
                    bypassOnDebug: true, // webpack@1.x
                    disable: true, // webpack@2.x and newer
                }
                }
            ]
         }
      ]
   }
}
module.exports = config;